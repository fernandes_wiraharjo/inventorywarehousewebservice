package adapter;

import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Base64Adapter {
	public static String EncriptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] encodedBytes = Base64.getEncoder().encode(lKeyword.getBytes());
			lResult = new String(encodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EncriptBase64", "" , "");
		}
		return lResult;
	}
	
	public static String DecriptBase64(String lKeyword){
		String lResult = "";
		try{
			byte[] decodedBytes = Base64.getDecoder().decode(lKeyword);
			lResult = new String(decodedBytes);
		}
		catch (Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "EncriptBase64", "" , "");
		}
		return lResult;
	}
	
	public static String getHash(String txt, String hashType) {
        try {
                    java.security.MessageDigest md = java.security.MessageDigest.getInstance(hashType);
                    byte[] array = md.digest(txt.getBytes());
                    StringBuffer sb = new StringBuffer();
                    for (int i = 0; i < array.length; ++i) {
                        sb.append(Integer.toHexString((array[i] & 0xFF) | 0x100).substring(1,3));
                 }
                    return sb.toString();
            } catch (java.security.NoSuchAlgorithmException e) {
                //error action
            }
            return null;
    }
	
	public static String get_SHA_512_SecurePassword(String passwordToHash){
		String generatedPassword = null;
		String salt = "Invent2016";
		    try {
		    	java.security.MessageDigest md = java.security.MessageDigest.getInstance("SHA-512");
		         md.update(salt.getBytes(StandardCharsets.UTF_8));
		         byte[] bytes = md.digest(passwordToHash.getBytes(StandardCharsets.UTF_8));
		         StringBuilder sb = new StringBuilder();
		         for(int i=0; i< bytes.length ;i++){
		            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
		         }
		         generatedPassword = sb.toString();
		        } 
		       catch (NoSuchAlgorithmException e){
		        e.printStackTrace();
		       }
		    return generatedPassword;
		}
}
