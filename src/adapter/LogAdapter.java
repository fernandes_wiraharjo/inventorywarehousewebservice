package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import java.util.Date;
import model.Globals;

/** Documentation
 * 
 */
public class LogAdapter {
	
	public static void InsertLogExc(String lException,String lKey,String lQuery,String lCreated_by)
	{
		try 
		{	
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date Created_On = new Date();
			String lCreated_On = df.format(Created_On);
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();	
			
			jrs.setCommand("SELECT * FROM log_exception LIMIT 1");
			jrs.execute();
			
			jrs.moveToInsertRow();
			jrs.updateString("Exception",lException);
			jrs.updateString("Query", lQuery);
			jrs.updateString("Key", lKey);
			jrs.updateString("Created_on", lCreated_On);
			jrs.updateString("Created_by", lCreated_by);
			
			jrs.insertRow();
		
			jrs.close();
			
			Globals.gReturn_Status = "Success insert log";
		}
		catch (Exception ex){
			Globals.gReturn_Status = ex.toString();
		}

		return;
	}
	
}
