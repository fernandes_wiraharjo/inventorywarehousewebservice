package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

/** Documentation
 * 001 Nanda -- Beginning
 */
public class ProductUomAdapter {

	public static String GetBaseUombyProductId(String lProductID) throws Exception {
//		List<model.mdlProductUom> listmdlProductUom = new ArrayList<model.mdlProductUom>();
		String lUOM = "";
		Connection connection = database.RowSetAdapter.getConnection();
		PreparedStatement pstmLoadProductUOM = null;
		ResultSet jrs = null;
		try{
			
			String sqlLoadProductUOM = "SELECT BaseUOM "
					+ "FROM product_uom WHERE ProductID = ? LIMIT 1";
			
			Globals.gCommand = sqlLoadProductUOM;
			
			pstmLoadProductUOM = connection.prepareStatement(sqlLoadProductUOM);
			pstmLoadProductUOM.setString(1,  lProductID);
			jrs = pstmLoadProductUOM.executeQuery();
									
			while(jrs.next()){
//				model.mdlProductUom mdlProductUOM = new model.mdlProductUom();
				lUOM = jrs.getString("BaseUOM");
//				listmdlProductUom.add(mdlProductUOM);
			}
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "GetBaseUombyProductId", Globals.gCommand , Globals.user);
		}
		finally {
			 if (pstmLoadProductUOM != null) {
				 pstmLoadProductUOM.close();
			 }
			 if (connection != null) {
				 connection.close();
			 }
			 if (jrs != null) {
				 jrs.close();
			 }
		 }

		return lUOM;
	}

}
