package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;

import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;
import model.mdlClosingPeriod;


public class ClosingPeriodAdapter {

	public static model.mdlClosingPeriod LoadActivePeriod () {
		model.mdlClosingPeriod mdlClosingPeriod = new model.mdlClosingPeriod();
		try{
			
			Class.forName("com.mysql.jdbc.Driver");
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT Period,Fiscal_Year,IsActive,Created_by,Created_at FROM closing_period WHERE IsActive=1 ORDER BY Created_at DESC LIMIT 1");
			
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			
			while(jrs.next()){

				mdlClosingPeriod.Period = jrs.getString("Period");
				mdlClosingPeriod.Fiscal_Year = jrs.getString("Fiscal_Year");
				mdlClosingPeriod.IsActive = jrs.getString("IsActive");
				mdlClosingPeriod.Created_by = jrs.getString("Created_by");
				mdlClosingPeriod.Created_at = jrs.getString("Created_at");

			}
			
			jrs.close();
			
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadActivatePeriod", Globals.gCommand , Globals.user);
		}
		
		return mdlClosingPeriod;
	}
	
}
