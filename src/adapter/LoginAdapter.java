package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;
import database.RowSetAdapter;
import model.Globals;
import model.mdlLogin;
import adapter.Base64Adapter;

public class LoginAdapter {
	
	public static String UpdateToken(mdlLogin mdlLogin) 
	{	
			PreparedStatement pstmUpdateToken = null;
			Connection connection = null;
			String sqlUpdateToken = "UPDATE user_login SET Token = ? ,TokenExpired = ?"
									+ "WHERE UserId = ? AND `Password` = ? AND Type='API' AND StatusToken = 'AVAILABLE'";
			
			try{
				connection = database.RowSetAdapter.getConnection();
				pstmUpdateToken = connection.prepareStatement(sqlUpdateToken);
				pstmUpdateToken.setString(1,  ValidateNull.NulltoStringEmpty(mdlLogin.token));
				pstmUpdateToken.setString(2,  LocalDateTime.now().plusDays(1).toString());
				pstmUpdateToken.setString(3,  ValidateNull.NulltoStringEmpty(mdlLogin.userId));
				pstmUpdateToken.setString(4,  ValidateNull.NulltoStringEmpty(Base64Adapter.EncriptBase64(mdlLogin.password)));

				pstmUpdateToken.executeUpdate();
				
				Globals.gReturn_Status = "SuccessUpdateToken";
			}
			
			catch (Exception e ) 
			{
            	LogAdapter.InsertLogExc(e.toString(), "UpdateToken", Globals.gCommand , Globals.user);
    			Globals.gReturn_Status = "Database Error";
		    }
			finally {
				try{
					if (pstmUpdateToken != null) {
		        		pstmUpdateToken.close();
		        	}
		        	if (connection != null) {
		        		connection.close();
		        	}
				}
				catch (Exception e)
				{
				
				}
		    }
			
			return Globals.gReturn_Status;
	}
	
	
	public static Boolean CheckToken(String lToken)
	{	
		Boolean lCheck = false;
		String[] lUserId = lToken.split("-");
		String lUsernameDecry = Base64Adapter.DecriptBase64(lUserId[0]);
		try {			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT UserId FROM user_login WHERE UserId = ? AND Token = ? AND Type = 'API' AND TokenExpired >= ? AND StatusToken = 'AVAILABLE' LIMIT 1");		
			jrs.setString(1,  lUsernameDecry);
			jrs.setString(2,  lToken);
			jrs.setString(3, LocalDateTime.now().toString());
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
			lCheck = jrs.next();

		    jrs.close();   
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(),"WS-" + "CheckToken", Globals.gCommand , Globals.user);
		}	
		
		return lCheck;
		
	}
	
	public static boolean ValidasiLogin(String lUserId, String lPassword)
	{
		boolean lCheck = false;
		String lPasswordEnc = Base64Adapter.EncriptBase64(lPassword);
		try {			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			jrs.setCommand("SELECT UserId,Password FROM user_login WHERE UserId = ? AND Password = ? LIMIT 1");		
			jrs.setString(1, lUserId);
			jrs.setString(2, lPasswordEnc);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
		    lCheck = jrs.next();
    
		    jrs.close();   
		}
		catch(Exception ex) {
			LogAdapter.InsertLogExc(ex.toString(), "ValidasiLogin", Globals.gCommand , Globals.user);
		}	
		
		return lCheck;
		
	}

}
