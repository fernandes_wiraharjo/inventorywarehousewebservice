package adapter;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class TokenAdapter {
	
	public static String GenerateToken(String lUsername, String lPassword){
		//Session dibuat 1 hari
		DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		String lDateNow = dtf.format(LocalDate.now()).toString();
		String lToken = "";
		String lTokenResult = "";
		String lUsernameDecry = Base64Adapter.EncriptBase64(lUsername);
		lToken += Base64Adapter.get_SHA_512_SecurePassword(lPassword);
		lToken += Base64Adapter.get_SHA_512_SecurePassword(lDateNow);
		lTokenResult = lUsernameDecry + "-" + Base64Adapter.EncriptBase64(lToken);
		return lTokenResult;
	}
}
