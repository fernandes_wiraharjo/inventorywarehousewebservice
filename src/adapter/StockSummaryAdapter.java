	package adapter;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.sql.rowset.JdbcRowSet;
import com.sun.rowset.JdbcRowSetImpl;

import model.Globals;

public class StockSummaryAdapter {

	public static List<model.mdlStockSummary> LoadQtyStockSummary(String lProductID, String lAcronymType) {
		List<model.mdlStockSummary> listmdlStockSummary = new ArrayList<model.mdlStockSummary>();
		try{
			String lQuery = "";
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			if (lAcronymType.contentEquals("FIFO")){
				lQuery = "SELECT a.*,b.Expired_Date,b.GRDate FROM stock_summary a "
						+ "INNER JOIN batch b ON a.Batch_No = b.Batch_No "
						+ "INNER JOIN (SELECT ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
									+ "FROM stock_summary "
									+ "WHERE PlantID='1002' AND WarehouseID='Website' AND ProductID=? AND Qty != 0 "
									+ "GROUP BY ProductID,Batch_No) c ON a.ProductID = c.ProductID AND a.Batch_No = c.Batch_No "
									+ "AND a.Updated_at = c.max_date AND a.PlantID = c.PlantID AND a.WarehouseID = c.WarehouseID "
						+ "ORDER BY b.GRDate";
			}
			else{
				lQuery = "SELECT a.*,b.Expired_Date,b.GRDate FROM stock_summary a "
						+ "INNER JOIN batch b ON a.Batch_No = b.Batch_No "
						+ "INNER JOIN (SELECT ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
									+ "FROM stock_summary "
									+ "WHERE PlantID='1002' AND WarehouseID='Website' AND ProductID=? AND Qty != 0 "
									+ "GROUP BY ProductID,Batch_No) c ON a.ProductID = c.ProductID AND a.Batch_No = c.Batch_No "
									+ "AND a.Updated_at = c.max_date AND a.PlantID = c.PlantID AND a.WarehouseID = c.WarehouseID "
						+ "ORDER BY b.Expired_Date";
			}
			
			jrs.setCommand(lQuery);
			
			jrs.setString(1,  lProductID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
								
			while(jrs.next()){				
				model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
				mdlStockSummary.Period = jrs.getString("Period");
				mdlStockSummary.ProductID = jrs.getString("ProductID");
				mdlStockSummary.PlantID = jrs.getString("PlantID");
				mdlStockSummary.WarehouseID = jrs.getString("WarehouseID");
				mdlStockSummary.Batch_No = jrs.getString("Batch_No");	
				mdlStockSummary.Qty = jrs.getString("Qty");
				mdlStockSummary.UOM = jrs.getString("UOM");
				
				listmdlStockSummary.add(mdlStockSummary);
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadStockSummary", Globals.gCommand , Globals.user);
		}

		return listmdlStockSummary;
	}
	
	
	public static int LoadTotalQtyStock(String lProductID) {
		int lQty = 0;
		try{
			
			JdbcRowSet jrs = new JdbcRowSetImpl();
			jrs = database.RowSetAdapter.getJDBCRowSet();
			
			//jrs.setCommand("SELECT SUM(Qty) as Qty FROM stock_summary WHERE `ProductID` = ? AND `PlantID` = '1002' AND `WarehouseID` = 'Website' GROUP BY period ORDER BY Period Desc LIMIT 1");
			jrs.setCommand("SELECT SUM(a.Qty) AS Qty FROM stock_summary a "
					+ "INNER JOIN (SELECT ProductID,Batch_No,PlantID,WarehouseID,MAX(Updated_at) max_date "
					+ "FROM stock_summary "
					+ "WHERE PlantID='1002' AND WarehouseID='Website' AND ProductID=? AND Qty != 0 "
					+ "GROUP BY ProductID,Batch_No) c ON a.ProductID = c.ProductID AND a.Batch_No = c.Batch_No "
					+ "AND a.Updated_at = c.max_date AND a.PlantID = c.PlantID AND a.WarehouseID = c.WarehouseID");
			jrs.setString(1,  lProductID);
			Globals.gCommand = jrs.getCommand();
			jrs.execute();
									
			while(jrs.next()){
				lQty = Integer.parseInt(jrs.getString("Qty"));				
			}
			jrs.close();		
		}
		catch(Exception ex){
			LogAdapter.InsertLogExc(ex.toString(), "LoadQtyStock", Globals.gCommand , Globals.user);
		}

		return lQty;
	}
	
	//Insert Transaction UpdateEcommerce
	public static List<model.mdlStockSummary> UpdateStockSummartECM(model.mdlParamStockECM mdlParamStockECM)
	{
		//Globals.gCommand = "Doc Number : " + mdlOutboundDetail.getDocNumber() + " Doc Line : " + mdlOutboundDetail.getDocLine();
		
		Connection connection = null;
		PreparedStatement pstmOutbound = null;
		PreparedStatement pstmOutboundDetail = null;
		PreparedStatement pstmStockSummary = null;
		
		boolean lCheckFinish = true;
		
		model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
		List<model.mdlStockSummary> mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
		
		String sqlOutbound = "INSERT INTO outbound(`DocNumber`, `Date`, `CustomerID`, `PlantID`, `WarehouseID`, `IsCancel`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?);";
		String sqlOutboundDetail = "INSERT INTO outbound_detail(`DocNumber`, `DocLine`, `ProductID`, `Qty_UOM`, `UOM`, `Qty_BaseUOM`, `BaseUOM`, `Batch_No`, `Packing_No`, `Created_by`, `Created_at`, `Updated_by`, `Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?);";
//		String sqlStockSummary = "UPDATE `stock_summary` SET `Qty`=? , `Updated_by`=?, `Updated_at`=? WHERE `Period`=? AND `ProductID`=? AND `PlantID`=? AND `WarehouseID`=? AND `Batch_No`=?;";
		String sqlStockSummary = "INSERT INTO stock_summary(`Period`,`ProductID`,`PlantID`,`WarehouseID`,`Batch_No`,`Qty`,`UOM`,`Created_by`,`Created_at`,`Updated_by`,`Updated_at`) VALUES (?,?,?,?,?,?,?,?,?,?,?) ON DUPLICATE KEY UPDATE `Qty`=?,`UOM`=?,`Updated_by`=?,`Updated_at`=?;";
			
		try{			
			
			connection = database.RowSetAdapter.getConnection();
			connection.setAutoCommit(false);
			
			pstmOutbound = connection.prepareStatement(sqlOutbound);
			pstmOutboundDetail = connection.prepareStatement(sqlOutboundDetail);
			pstmStockSummary = connection.prepareStatement(sqlStockSummary);
			
			//parameter outbound
			pstmOutbound.setString(1,  ValidateNull.NulltoStringEmpty(mdlParamStockECM.invoice));
			pstmOutbound.setString(2,  LocalDateTime.now().toString());
			pstmOutbound.setString(3,  mdlParamStockECM.customerid);
			pstmOutbound.setString(4,  "1002");
			pstmOutbound.setString(5,  "Website");
			pstmOutbound.setString(6,  "0");
			pstmOutbound.setString(7,  Globals.user);
			pstmOutbound.setString(8,  LocalDateTime.now().toString());
			pstmOutbound.setString(9,  Globals.user);
			pstmOutbound.setString(10,  LocalDateTime.now().toString());
			pstmOutbound.executeUpdate();
			
			int DocLine = 0;
			for (model.mdlProductDetail mdlParamProductDetail : mdlParamStockECM.ProductDetailList) 
			{
				String lProduct = mdlParamProductDetail.product_id +"-"+ mdlParamProductDetail.id;
				model.mdlClosingPeriod CheckActivatePeriod = ClosingPeriodAdapter.LoadActivePeriod();
				String lPeriod = CheckActivatePeriod.Period;	
				String lUOM = ProductUomAdapter.GetBaseUombyProductId(lProduct);
				mdlStockSummary = new model.mdlStockSummary();
				
				int lTotalQtySummary = StockSummaryAdapter.LoadTotalQtyStock(lProduct);
				if(lTotalQtySummary > Integer.parseInt(mdlParamProductDetail.quantity)) //Cek jika Total Qty lebih Besar dari quantity
				{	
					
					List<model.mdlStockSummary> mdlStockSummarylist2 = new ArrayList<model.mdlStockSummary>();
					
					mdlStockSummarylist2 =  LoadQtyStockSummary(lProduct,mdlParamProductDetail.method);
					int lHasilQty = 0;
					int lQtyECM = Integer.parseInt(mdlParamProductDetail.quantity);
					for(model.mdlStockSummary mdlStockSummary2 : mdlStockSummarylist2) //looping untuk kurangi kuantiti
					{
						
						int lQty = Integer.parseInt(mdlStockSummary2.Qty);
						
						
						if (lQtyECM == 0){
							lCheckFinish = false;
							break;
						}
						else
						{
							if (lQty >= lQtyECM){
								lHasilQty = lQty - lQtyECM;
//								//
//								//parameter stock_summary
//								pstmStockSummary.setString(1,  String.valueOf(lHasilQty));
//								pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(Globals.user));
//								pstmStockSummary.setString(3,  LocalDateTime.now().toString());
//								pstmStockSummary.setString(4,  ValidateNull.NulltoStringEmpty(lPeriod));
//								pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(lProduct));
//								pstmStockSummary.setString(6,  "Plant002");
//								pstmStockSummary.setString(7,  "Website");
//								pstmStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.Batch_No));
//								pstmStockSummary.executeUpdate();
								
								//Parameter untuk insert update
								//parameter stock_summary
								pstmStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(lPeriod));
								pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lProduct));
								pstmStockSummary.setString(3,  "1002");
								pstmStockSummary.setString(4,  "Website");
								pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.Batch_No));
								pstmStockSummary.setString(6,  String.valueOf(lHasilQty));
								pstmStockSummary.setString(7,  lUOM);
								pstmStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.setString(12,  String.valueOf(lHasilQty));
								pstmStockSummary.setString(13,  lUOM);
								pstmStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.executeUpdate();
								
								mdlStockSummary = new model.mdlStockSummary();
								mdlStockSummary.Result = "Success";
								mdlStockSummary.Period = lPeriod;
								mdlStockSummary.Batch_No = mdlStockSummary2.Batch_No;
								mdlStockSummary.PlantID = "1002";
								mdlStockSummary.ProductID = lProduct;
								mdlStockSummary.Qty = String.valueOf(lHasilQty);
								mdlStockSummary.UOM = lUOM;
								mdlStockSummary.WarehouseID = "Website";													
								mdlStockSummarylist.add(mdlStockSummary);
								//------------------------
								
								
								//parameter outbound detail
								DocLine += 1;
								pstmOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(mdlParamStockECM.invoice));
								pstmOutboundDetail.setString(2,  String.valueOf(DocLine));
								pstmOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lProduct));
								pstmOutboundDetail.setString(4,  String.valueOf(lQtyECM));
								pstmOutboundDetail.setString(5,  lUOM);
								pstmOutboundDetail.setString(6,  String.valueOf(lQtyECM));
								pstmOutboundDetail.setString(7,  lUOM);
								pstmOutboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.Batch_No));
								pstmOutboundDetail.setString(9,  "Ecommerce_Packing");
								pstmOutboundDetail.setString(10,  Globals.user);
								pstmOutboundDetail.setString(11,  LocalDateTime.now().toString());
								pstmOutboundDetail.setString(12,  Globals.user);
								pstmOutboundDetail.setString(13,  LocalDateTime.now().toString());
								pstmOutboundDetail.executeUpdate();
								
								break;
							}
							else
							{
								lQtyECM = lQtyECM - lQty;
								
								//Parameter untuk insert update
								//parameter stock_summary
								pstmStockSummary.setString(1,  ValidateNull.NulltoStringEmpty(lPeriod));
								pstmStockSummary.setString(2,  ValidateNull.NulltoStringEmpty(lProduct));
								pstmStockSummary.setString(3,  "1002");
								pstmStockSummary.setString(4,  "Website");
								pstmStockSummary.setString(5,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.Batch_No));
								pstmStockSummary.setString(6,  "0");
								pstmStockSummary.setString(7,  lUOM);
								pstmStockSummary.setString(8,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(9,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.setString(10,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(11,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.setString(12,  "0");
								pstmStockSummary.setString(13,  lUOM);
								pstmStockSummary.setString(14,  ValidateNull.NulltoStringEmpty(Globals.user));
								pstmStockSummary.setString(15,  ValidateNull.NulltoStringEmpty(LocalDateTime.now().toString()));
								pstmStockSummary.executeUpdate();
								
								mdlStockSummary = new model.mdlStockSummary();
								mdlStockSummary.Result = "Success";
								mdlStockSummary.Period = lPeriod;
								mdlStockSummary.Batch_No = mdlStockSummary2.Batch_No;
								mdlStockSummary.PlantID = "1002";
								mdlStockSummary.ProductID = lProduct;
								mdlStockSummary.Qty = "0";
								mdlStockSummary.UOM = lUOM;
								mdlStockSummary.WarehouseID = "Website";													
								mdlStockSummarylist.add(mdlStockSummary);
								
								//parameter outbound detail
								DocLine += 1;
								pstmOutboundDetail.setString(1,  ValidateNull.NulltoStringEmpty(mdlParamStockECM.invoice));
								pstmOutboundDetail.setString(2,  String.valueOf(DocLine));
								pstmOutboundDetail.setString(3,  ValidateNull.NulltoStringEmpty(lProduct));
								pstmOutboundDetail.setString(4,  String.valueOf(lQty));
								pstmOutboundDetail.setString(5,  lUOM);
								pstmOutboundDetail.setString(6,  String.valueOf(lQty));
								pstmOutboundDetail.setString(7,  lUOM);
								pstmOutboundDetail.setString(8,  ValidateNull.NulltoStringEmpty(mdlStockSummary2.Batch_No));
								pstmOutboundDetail.setString(9,  "Ecommerce_Packing");
								pstmOutboundDetail.setString(10,  Globals.user);
								pstmOutboundDetail.setString(11,  LocalDateTime.now().toString());
								pstmOutboundDetail.setString(12,  Globals.user);
								pstmOutboundDetail.setString(13,  LocalDateTime.now().toString());
								pstmOutboundDetail.executeUpdate();

							}
						}	
					}					
					//>>
					
					
				}
				else
				{
					mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
					mdlStockSummary.Period = lPeriod;
					mdlStockSummary.Batch_No = "";
					mdlStockSummary.PlantID = "1002";
					
					mdlStockSummary.ProductID = lProduct;
					mdlStockSummary.Qty = "";
					mdlStockSummary.UOM = lUOM;
					mdlStockSummary.WarehouseID = "Website";																		
					mdlStockSummary.Result = "Kuantiti produk yang Anda masukkan lebih besar dari kuantiti produk yang tersedia";	
					mdlStockSummarylist.add(mdlStockSummary);
					lCheckFinish = false;
					break;
				}
			}
			
			if(lCheckFinish == true)
				connection.commit();
			else
				connection.rollback();
			
		}catch (Exception e ) {
	        if (connection != null) {
	            try {
	                System.err.print("Transaction is being rolled back");
	                connection.rollback();
	            } catch(SQLException excep) {
	            	LogAdapter.InsertLogExc(excep.toString(), "UpdateStockSummartECM", Globals.gCommand , Globals.user);
	            	mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
	            	mdlStockSummary.Result = "Error Database";	
					mdlStockSummarylist.add(mdlStockSummary);
	            }
	            LogAdapter.InsertLogExc(e.toString(), "UpdateStockSummartECM", Globals.gCommand , Globals.user);
	            mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
	            mdlStockSummary.Result = "Error Database";	
				mdlStockSummarylist.add(mdlStockSummary);
	        }
		}finally 
		{
			try{
				if (pstmOutbound != null) {
					pstmOutbound.close();
				}
				if (pstmOutboundDetail != null) {
					pstmOutboundDetail.close();
				}
				if (pstmStockSummary != null) {
					pstmStockSummary.close();
				}
				connection.setAutoCommit(true);
				if (connection != null) {
					connection.close();
				}
				//sampe bikin try catch
			}
			catch (Exception e ) 
			{
			}
		}
		return mdlStockSummarylist;
	}
	
	
}
