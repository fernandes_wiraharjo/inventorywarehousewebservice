package com.invent.controller;

import java.util.ArrayList;
import java.util.List;

//import javax.ws.rs.core.Response;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

//import com.google.gson.Gson;
import model.Globals;
import adapter.LogAdapter;
import adapter.LoginAdapter;
import adapter.StockSummaryAdapter;
import adapter.TokenAdapter;
import adapter.ValidateNull;

@RestController
public class controller {
	
	@RequestMapping(value = "/ping",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody List<model.mdlStockSummary> GetPing(@RequestBody model.mdlParamStockECM mdlParamStockECM)
	{	
		Boolean lCheck = false;
		List<model.mdlStockSummary> mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
		model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
		
		lCheck = LoginAdapter.CheckToken(mdlParamStockECM.Token);
		if(lCheck){
			mdlStockSummary.Result = "Token is valid";
			mdlStockSummarylist.add(mdlStockSummary);
		}
		else{
			mdlStockSummary.Result = "Token is Invalid";
			mdlStockSummarylist.add(mdlStockSummary);
		}
		return mdlStockSummarylist;
	}
	
	@RequestMapping(value = "/getjson",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlParamStockECM GetJsonbyModel()
	{	
		List<model.mdlProductDetail> mdlProductDetaillist = new ArrayList<model.mdlProductDetail>();
		model.mdlProductDetail mdlParamProductDetail = new model.mdlProductDetail();
		
		model.mdlParamStockECM mdlParamStockECM = new model.mdlParamStockECM();
		mdlParamStockECM.Token = "ZWQzZWNjNThlZjVjY2Q3YWY0MDlkZWY3OGE0N2RmMWM=ZWQzZWNjNThlZjVjY2Q3YWY0MDlkZWY3OGE0N2RmMWM=YTg1MjY0YmI5NGM3YTk0ZjQ5OWEzMGY4Yzk0ZjcwMmU=";
		mdlParamStockECM.invoice = "ECM180001";
		mdlParamStockECM.customerid = "CST180001";
		
		mdlParamProductDetail.id = "1000";
		mdlParamProductDetail.product_id = "344";
		mdlParamProductDetail.quantity = "1";
		mdlParamProductDetail.method = "FIFO";
		mdlProductDetaillist.add(mdlParamProductDetail);
		
		mdlParamProductDetail = new model.mdlProductDetail();
		mdlParamProductDetail.id = "1000";
		mdlParamProductDetail.product_id = "345";
		mdlParamProductDetail.quantity = "1";
		mdlParamProductDetail.method = "FIFO";
		mdlProductDetaillist.add(mdlParamProductDetail);
		
		model.mdlUser mdlUser = new model.mdlUser();
		mdlUser.Password = "MTIz";
		mdlUser.Username = "nandadwisubekti@gmail.com";
		
		mdlParamStockECM.ProductDetailList = mdlProductDetaillist;
		
		return mdlParamStockECM;
	}
	
	//	Create API Token Temporary
	@RequestMapping(value = "/token",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody model.mdlToken GetToken(@RequestBody model.mdlUser param)
	{	
		model.mdlLogin mdlLogin = new model.mdlLogin();
		model.mdlToken lToken = new model.mdlToken();
		String lUsername = ValidateNull.NulltoStringEmpty(param.Username);
		String lPassword = ValidateNull.NulltoStringEmpty(param.Password);
		
		try 
		{
			if (lUsername.equals("") || lPassword.equals(""))			
				lToken.token = "Error Username is Empty OR Password is Empty";			
			else
			{
				if(LoginAdapter.ValidasiLogin(lUsername, lPassword)) {
					lToken.token = TokenAdapter.GenerateToken(lUsername, lPassword).toString();

					mdlLogin.userId = lUsername;
					mdlLogin.password = lPassword;
					mdlLogin.token = lToken.token;
				
					String Result = LoginAdapter.UpdateToken(mdlLogin);		
					if(!Result.contains("SuccessUpdateToken"))
					lToken.token = "Error Generate token";
				}
				else
				{
					lToken.token = "Error Username AND Password is Invalid";	
				}
			}
		} catch (Exception e) {
			lToken.token = "Error Generate token";
		}	    
		return 	lToken;
	}

	// Create Mengurangi Stock Summary
	@RequestMapping(value = "/updatestock",method = RequestMethod.POST,consumes="application/json",headers="Accept=application/json")
	public @ResponseBody List<model.mdlStockSummary> UpdateStockSummary(@RequestBody model.mdlParamStockECM mdlParamStockECM)
	{	
		Boolean lCheck = false;
		List<model.mdlStockSummary> mdlStockSummarylist = new ArrayList<model.mdlStockSummary>();
		model.mdlStockSummary mdlStockSummary = new model.mdlStockSummary();
		lCheck = LoginAdapter.CheckToken(mdlParamStockECM.Token);
		if(lCheck)
			mdlStockSummarylist = StockSummaryAdapter.UpdateStockSummartECM(mdlParamStockECM);
		else{
			mdlStockSummary.Result = "Token is Invalid";
			mdlStockSummarylist.add(mdlStockSummary);
		}
		
		return 	mdlStockSummarylist;
	}
}
